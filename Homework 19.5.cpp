﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voise()
    {
        cout << "sound" << endl;
    }
};

class Dragon : public Animal
{
public:
    void Voise() override
    {
        cout << "Graaaawr" << endl;
    }
};

class Frog : public Animal
{
public:
    void Voise() override
    {
        cout << "Ribbit-ribbit" << endl;
    }
};

class Snake : public Animal
{
public:
    void Voise() override
    {
        cout << "Hsssss" << endl;
    }
};

int main()
{
    Animal* array[3];

    array[0] = new Dragon;
    array[1] = new Frog;
    array[2] = new Snake;

    for (size_t i = 0; i < 3; i++)
    {
        array[i]->Voise();
    }
}
